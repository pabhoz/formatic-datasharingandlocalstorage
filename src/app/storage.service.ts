import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService as Storage } from 'angular-webstorage-service';

//Llave de almacenamiento
const STORAGE_KEY = 'deudas';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  deudas = [];
  constructor(@Inject(LOCAL_STORAGE) private storage: Storage) { }

  public save(deuda: any): void {
    const deudas = this.storage.get(STORAGE_KEY) || [];
    deudas.push(deuda);
    this.storage.set(STORAGE_KEY, deudas);
  }

  public get(){
    return this.storage
      .get(STORAGE_KEY) || 'LocaL storage is empty';
  }
}
