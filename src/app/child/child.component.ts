import{ Component, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-child',
    template: `
        <div>{{content}}</div>
        <div (click)="emit()">Yell to parent</div>
    `,
    styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
    
  childValue: string = "Hey pa!"

  @Output() childIsYelling = new EventEmitter<string>();
  
  constructor() { }  
     
  ngOnInit(){
    
  }

  emit(){
    this.childIsYelling.emit(this.childValue);
  }

}